# OpenML dataset: BachChoralHarmony

https://www.openml.org/d/4552

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: -- Creators: Daniele P. Radicioni and Roberto Esposito  -- Donor: Daniele P. Radicioni (radicion '@' di.unito.it) and Roberto Esposito (esposito '@' di.unito.it)  -- Date: May","2014  
**Source**: UCI  
**Please cite**: D. P. Radicioni and R. Esposito. Advances in Music Information Retrieval, chapter BREVE: an HMPerceptron-Based Chord Recognition System. Studies in Computational Intelligence, Zbigniew W. Ras and Alicja Wieczorkowska (Editors), Springer, 2010.  

Abstract: The data set is composed of 60 chorales (5665 events) by J.S. Bach (1675-1750). Each event of each chorale is labelled using 1 among 101 chord labels and described through 14 features.
Source:

-- Creators: Daniele P. Radicioni and Roberto Esposito 
-- Donor: Daniele P. Radicioni (radicion '@' di.unito.it) and Roberto Esposito (esposito '@' di.unito.it) 
-- Date: May, 2014


Data Set Information:

Pitch classes information has been extracted from MIDI sources downloaded 
from (JSB Chorales)[[Web Link]]. Meter information has 
been computed through the Meter program which is part of the Melisma 
music analyser (Melisma)[[Web Link]]. 
Chord labels have been manually annotated by a human expert.


Attribute Information:

1. Choral ID: corresponding to the file names from (Bach Central)[[Web Link]]. 
2. Event number: index (starting from 1) of the event inside the chorale. 
3-14. Pitch classes: YES/NO depending on whether a given pitch is present. 
Pitch classes/attribute correspondence is as follows: 
C -&gt; 3 
C#/Db -&gt; 4 
D -&gt; 5 
... 
B -&gt; 14 
15. Bass: Pitch class of the bass note 
16. Meter: integers from 1 to 5. Lower numbers denote less accented events, 
higher numbers denote more accented events. 
17. Chord label: Chord resonating during the given event.


Relevant Papers:

1. D. P. Radicioni and R. Esposito. Advances in Music Information Retrieval, 
chapter BREVE: an HMPerceptron-Based Chord Recognition System. Studies 
in Computational Intelligence, Zbigniew W. Ras and Alicja Wieczorkowska 
(Editors), Springer, 2010. 
2. Esposito, R. and Radicioni, D. P., CarpeDiem: Optimizing the Viterbi 
Algorithm and Applications to Supervised Sequential Learning, Journal 
of Machine Learning Research, 10(Aug):1851-1880, 2009.



Citation Request:

D. P. Radicioni and R. Esposito. Advances in Music Information Retrieval, chapter BREVE: an HMPerceptron-Based Chord Recognition System. Studies in Computational Intelligence, Zbigniew W. Ras and Alicja Wieczorkowska (Editors), Springer, 2010.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4552) of an [OpenML dataset](https://www.openml.org/d/4552). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4552/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4552/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4552/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

